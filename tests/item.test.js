const app = require("../app");
const request = require('supertest');

describe("API /items", () => {

    it("test get /items", (done) => {
    request(app)
        .get("/items")
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            console.log(response.body)
            const firstData = response.body[0]
            expect(firstData.id).toBe(1)

            done();
        })

        
        .catch(done)
    })

    it("test get /items/:id", (done) => {
        request(app)
            .get("/items/2")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                console.log(response.body)
                const firstData = response.body[0]
                expect(data.id).toBe(2)
                expect(data.name).toBe(Buku A3)
                expect(data.status).toBe(active)
                expect(data.type).toBe(Alat Belajar)
                done();
            })
        
})

it("test post /items", (done) => {
    request(app)
        .post("/items")
        .send({ name: "Keyboard", status: "active", type: "Electronic"})
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            console.log(response.body)
            const firstData = response.body[0]
            expect(data.id).toBe(2)
            expect(data.name).toBe(Buku A3)
            expect(data.status).toBe(active)
            expect(data.type).toBe(Alat Belajar)
            done();
        })
    
})

})