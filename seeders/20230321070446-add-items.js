'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

  await queryInterface.bulkInsert('Items', [
    {
      name: "Pensil", 
      status: "active",
      type: "Alat Belajar",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: "Buku A3", 
      status: "active",
      type: "Alat Belajar",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: "Handphone", 
      status: "active",
      type: "Electronic",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: "Sepatu", 
      status: "active",
      type: "Fashion",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: "Rolex", 
      status: "active",
      type: "Watch",
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Items', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
